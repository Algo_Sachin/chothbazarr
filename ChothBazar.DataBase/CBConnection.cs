﻿using ChothBazar.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChothBazar.DataBase
{
    class CBConnection: DbContext
    {
        public CBConnection() : base("ChothBazarConnection")
            {
            }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categoies { get; set; }
    }
}
